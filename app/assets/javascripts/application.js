// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require blueimp-gallery/js/blueimp-helper.js
//= require blueimp-gallery/js/blueimp-gallery.min.js
//= require blueimp-gallery/js/blueimp-gallery-fullscreen.js
//= require blueimp-gallery/js/blueimp-gallery-indicator.js
//= require blueimp-gallery/js/jquery.blueimp-gallery.min.js
//= require d3/d3.js
//= require canvas-toBlob.js/canvas-toBlob.js
//= require FileSaver.js/FileSaver.js
//= require d3-tip/index.js
//= require_tree .
