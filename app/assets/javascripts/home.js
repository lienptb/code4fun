$(function () {
  'use strict';
  if($("#links").length){
    // Load demo images from flickr:
    $.ajax({
      // Flickr API is SSL only:
      // https://code.flickr.net/2014/04/30/flickr-api-going-ssl-only-on-june-27th-2014/
      url: 'https://api.flickr.com/services/rest/',
      data: {
        format: 'json',
        method: 'flickr.interestingness.getList',
        api_key: '7617adae70159d09ba78cfec73c13be3' // jshint ignore:line
      },
      dataType: 'jsonp',
      jsonp: 'jsoncallback'
    }).done(function (result) {
      var linksContainer = $('#links');
      var baseUrl;
      // Add the demo images as links with thumbnails to the page:
      $.each(result.photos.photo, function (index, photo) {
        baseUrl = 'https://farm' + photo.farm + '.static.flickr.com/' +
        photo.server + '/' + photo.id + '_' + photo.secret;
        $('<a/>')
          .append($('<img>').prop('src', baseUrl + '_s.jpg'))
          .prop('href', baseUrl + '_b.jpg')
          .prop('title', photo.title)
          .attr('data-gallery', '')
          .appendTo(linksContainer);
      });
    });
  }
  
  if($("#chart").length){
    // JS for Chart
    
    // Get dates between date range
    var getDates = function(minDate, maxDate){
      var currentDate = minDate,
          between = [];
      
      do {
        between.push(currentDate.getDate() + "/" + (currentDate.getMonth() + 1));
        currentDate.setDate(currentDate.getDate() + 1);
      }while(currentDate <= maxDate);
      
      return between;
    };
    
    // Mouseover function
    function mover(d) {
      var el = d3.select(this)
        .transition()
        .duration(10)     
        .style("fill-opacity", 0.3)
        ;
      tip.html("<strong>Day:</strong> <span>" + d.day + "</span><br/><strong>Hour:</strong> <span>" + d.hour + "</span><br/><strong>Utilization:</strong> <span style='color:red'>" + d.value + "</span>").show();
    }
    
    // Mouseout function
    function mout(d) { 
      var el = d3.select(this)
         .transition()
         .duration(1000)
         .style("fill-opacity", 1)
         ;
     tip.hide();
    };

    var minDate = new Date("08/25/2015"),
        maxDate = new Date("09/02/2015"),
        times = ["01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00"];
    var days = getDates(minDate, maxDate);
    
    var highestPoint = [[1, 15, 70], [2, 14, 65], [3, 17, 60], [4, 15, 64], [5, 16, 67], [6, 11, 14], [7, 22, 19], [8, 11, 14], [9, 22, 19]];
    
    var margin = { top: 50, right: 0, bottom: 100, left: 100 },
        width = 430 - margin.left - margin.right,
        height = 660 - margin.top - margin.bottom,
        gridSize = Math.floor(height / times.length),
        legendElementWidth = gridSize*2,
        buckets = 9,
        colors = ["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"], // alternatively colorbrewer.YlGnBu[9]
        datasets = ["/data/data.tsv", "/data/data2.tsv"];
        
    // Calculate for highest points line
    var highestLine = [];
    for (var i = 0; i < highestPoint.length; i++) {
      var x = ((highestPoint[i][0] - 1) * gridSize) + gridSize/2;
      var y = ((24 - highestPoint[i][1]) * gridSize) + gridSize/2;
      highestLine.push({x: x, y: y});
    }
       
    var chart = d3.select("#chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);
        
    var svg = chart.append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
    var dayLabels = svg.selectAll(".dayLabel")
        .data(days)
        .enter().append("text")
          .text(function (d, i) {
            if(i%5 == 0)
              return d; 
            else 
              return "";
          })
          .attr("x", function (d, i) { return i * gridSize; })
          .attr("y", 0)
          .style("text-anchor", "end")
          .attr("transform", "translate(" + gridSize + ", " + (height + 10) + ")")
          .attr("class", function (d, i) { return "dayLabel mono axis"; });
  
    var timeLabels = svg.selectAll(".timeLabel")
        .data(times)
        .enter().append("text")
          .text(function(d) { return d; })
          .attr("x", 0)
          .attr("y", function(d, i) { return (23 - i) * gridSize; })
          .style("text-anchor", "middle")
          .attr("transform", "translate(-30," + gridSize / 1.5 + ")")
          .attr("class", function(d, i) { return ((i >= 7 && i <= 16) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis"); });
          
    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0]);
      
    svg.call(tip);
  
    var heatmapChart = function(tsvFile) {
      d3.tsv(tsvFile,
      function(d) {
        return {
          day: +d.day,
          hour: +d.hour,
          value: +d.value
        };
      },
      function(error, data) {
        // Calculate to generate color
        var colorScale = d3.scale.quantile()
            .domain([0, buckets - 1, d3.max(data, function (d) { return d.value; })])
            .range(colors);
  
        var cards = svg.selectAll(".hour")
            .data(data, function(d) {return d.day+':'+d.hour;});
  
        cards.append("title");

        cards.enter().append("rect")
            .attr("x", function(d) { return (d.day - 1) * gridSize; })
            .attr("y", function(d) { return (24 - d.hour) * gridSize; })
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("class", "hour bordered")
            .attr("width", gridSize)
            .attr("height", gridSize)
            .style("fill", colors[0])
            .on('mouseover', mover)
            .on('mouseout', mout)
            .on('click', function(d,i){ 
              console.log('click');
              console.log(d);
            });
  
        cards.transition().duration(1000)
            .style("fill", function(d) { return colorScale(d.value); });
  
        cards.select("title").text(function(d) { return d.value; });
        
        cards.exit().remove();
  
        var legend = svg.selectAll(".legend")
            .data([0].concat(colorScale.quantiles()), function(d) { return d; });
  
        legend.enter().append("g")
            .attr("class", "legend");
  
        legend.append("rect")
          .attr("x", function(d, i) { return legendElementWidth * i; })
          .attr("y", height + 20)
          .attr("width", legendElementWidth)
          .attr("height", gridSize)
          .style("fill", function(d, i) { return colors[i]; });
  
        legend.append("text")
          .attr("class", "mono")
          .text(function(d) { return "≥ " + Math.round(d); })
          .attr("x", function(d, i) { return legendElementWidth * i + gridSize/2.5; })
          .attr("y", height + 25 + gridSize/3);
  
        legend.exit().remove();
      });  
    };
    heatmapChart(datasets[0]);
    
    // Draw the highest line
    var lineFunction = d3.svg.line()
                        .x(function(d) { return d.x; })
                        .y(function(d) { return d.y; })
                        .interpolate("linear");
    setTimeout(function(){
      svg.append("g").append("path")
        .attr("d", lineFunction(highestLine))
        .attr("stroke", "red")
        .attr("stroke-width", 2)
        .attr("fill", "none");
    }, 500);

    var datasetpicker = d3.select("#dataset-picker").selectAll(".dataset-button")
      .data(datasets);
  
    datasetpicker.enter()
      .append("input")
      .attr("value", function(d){ return "Dataset " + d; })
      .attr("type", "button")
      .attr("class", "dataset-button")
      .on("click", function(d) {
        heatmapChart(d);
      });
    // Set-up the export button
    d3.select('#saveButton').on('click', function(){
      console.log('click');
      console.log(chart.node());
      var svgString = getSVGString(chart.node());
      svgString2Image(svgString, 2*width, 2*height, 'png', save ); // passes Blob and filesize String to the callback
    
      function save(dataBlob,filesize){
        saveAs(dataBlob,'D3 vis exported to PNG.png'); // FileSaver.js function
      }
    });
    
    // Below are the function that handle actual exporting:
    // getSVGString (svgNode ) and svgString2Image( svgString, width, height, format, callback )
    function getSVGString( svgNode ) {
      svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
      var cssStyleText = getCSSStyles( svgNode );
      appendCSS( cssStyleText, svgNode );
    
      var serializer = new XMLSerializer();
      var svgString = serializer.serializeToString(svgNode);
      svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
      svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix
    
      return svgString;
    
      function getCSSStyles( parentElement ) {
        var selectorTextArr = [];
    
        // Add Parent element Id and Classes to the list
        selectorTextArr.push( '#'+parentElement.id );
        for (var c = 0; c < parentElement.classList.length; c++)
            if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
              selectorTextArr.push( '.'+parentElement.classList[c] );
    
        // Add Children element Ids and Classes to the list
        var nodes = parentElement.getElementsByTagName("*");
        for (var i = 0; i < nodes.length; i++) {
          var id = nodes[i].id;
          if ( !contains('#'+id, selectorTextArr) )
            selectorTextArr.push( '#'+id );
    
          var classes = nodes[i].classList;
          for (var c = 0; c < classes.length; c++)
            if ( !contains('.'+classes[c], selectorTextArr) )
              selectorTextArr.push( '.'+classes[c] );
        }
        // Extract CSS Rules
        var extractedCSSText = "";
        for (var i = 0; i < document.styleSheets.length; i++) {
          var s = document.styleSheets[i];
          
          try {
              if(!s.cssRules) continue;
          } catch( e ) {
                if(e.name !== 'SecurityError') throw e; // for Firefox
                continue;
              }
    
          var cssRules = s.cssRules;
          for (var r = 0; r < cssRules.length; r++) {
            if ( contains( cssRules[r].selectorText, selectorTextArr ) )
              extractedCSSText += cssRules[r].cssText;
          }
        }
        
        return extractedCSSText;
    
        function contains(str,arr) {
          return arr.indexOf( str ) === -1 ? false : true;
        }
    
      }
    
      function appendCSS( cssText, element ) {
        var styleElement = document.createElement("style");
        styleElement.setAttribute("type","text/css"); 
        styleElement.innerHTML = cssText;
        var refNode = element.hasChildNodes() ? element.children[0] : null;
        element.insertBefore( styleElement, refNode );
      }
    }
    
    
    function svgString2Image( svgString, width, height, format, callback ) {
      var format = format ? format : 'png';
    
      var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to dataurl
    
      var canvas = document.createElement("canvas");
      var context = canvas.getContext("2d");
    
      canvas.width = width;
      canvas.height = height;
      //set background color
      
      context.fillStyle = "white";
      //draw background / rect on entire canvas
      context.fillRect(0,0,width,height);
      
      context.fillStyle = 'black';
      context.font="40px Georgia";
      context.fillText("Utilization Chart",200,50);
      
      var image = new Image;
      image.onload = function() {
        context.drawImage(image, 0, 0, width, height);
        
        canvas.toBlob( function(blob) {
          var filesize = Math.round( blob.length/1024 ) + ' KB';
          if ( callback ) callback( blob, filesize );
        });
    
      };
    
      image.src = imgsrc;
      console.log(imgsrc);
    }
  }
  if($("#linechart").length){
    
    // Set the dimensions of the canvas / graph
    var margin = {top: 20, right: 80, bottom: 50, left: 30},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;
    
    // Parse the date / time
    var parseTime = d3.time.format("%Y%m%d").parse;
    
    // Set the ranges
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var z = d3.scale.category10();
    
    var line = d3.svg.line()
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.value); });
    
    var svg = d3.select("#linechart").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom),
        g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
    // Define the axes
    var xAxis = d3.svg.axis().scale(x)
      .orient("bottom");
    
    var yAxis = d3.svg.axis().scale(y)
      .orient("left");
      
    d3.tsv("/data/dataline.tsv", type, function(error, data) {
      if (error) throw error;
      
      var names = d3.keys(data[0]).filter(function(key) {
        return key !== "date";
      });
      
      var datas = names.map(function(id) {
        return {
          id: id,
          values: data.map(function(d) {
            return {date: d.date, value: +d[id], id: id};
          })
        };
      });
      
      var edges = [];
      datas.forEach(function(d){
        edges[d.id] = {
          min: d3.min(d.values, function(c) { return c.value; }),
          max: d3.max(d.values, function(c) { return c.value; })
        };
      });
      
      x.domain(d3.extent(data, function(d) { return d.date; }));

      y.domain([
        0,
        d3.max(datas, function(c) { return d3.max(c.values, function(d) { return d.value; }); })
      ]);
      
      z.domain(datas.map(function(c) { return c.id; }));
    
      function color(id, value){
        if(value == edges[id].min){
          return 'red';
        }
        if(value == edges[id].max){
          return 'green';
        }
          
        return z(id); 
      };
      function radius(id, value){
        if(value == edges[id].min || value == edges[id].max)
          return 5;
        return 2;
      }
      
      // Add the X Axis
      g.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
  
      // Add the Y Axis
      g.append("g")
        .attr("class", "y axis")
        .call(yAxis);
    
      var lineChart = g.selectAll(".value-type")
        .data(datas)
        .enter().append("g")
        .attr("class", "value-type");

      lineChart.append("path")
          .attr("class", "line")
          .attr("d", function(d) { return line(d.values); })
          .style("stroke", function(d) { return z(d.id); });
          
      // Add the scatterplot
      lineChart.selectAll("dot")
        .data(function(d) { return d.values; })
        .enter().append("circle")
        .attr("r", function(d){
          return radius(d.id, d.value);
        })
        .attr("cx", function(d) { return x(d.date); })
        .attr("cy", function(d) { return y(d.value); })
        .style("fill", function(d) { 
          return color(d.id, d.value);
        });
                
      var legend = svg.selectAll(".legend")
          .data(datas)
          .enter().append("g")
          .attr("class", "legend");

      legend.append("line")
        .attr("x1", function(d, i) {return i*150 + margin.left; })
        .attr("y1", height + margin.top + margin.bottom - 5)
        .attr("x2", function(d, i) {return i*150 + margin.left + 50; })
        .attr("y2", height + margin.top + margin.bottom - 5)
        .attr("stroke-width", 2)
        .style("stroke", function(d) { return z(d.id); });
        
      legend.append("circle")
        .attr("r", 2)
        .attr("cx", function(d, i) {return i*150 + margin.left; })
        .attr("cy", height + margin.top + margin.bottom - 5)
        .style("fill", function(d) { return z(d.id); });
        
      legend.append("circle")
        .attr("r", 2)
        .attr("cx", function(d, i) {return i*150 + margin.left + 50; })
        .attr("cy", height + margin.top + margin.bottom - 5)
        .style("fill", function(d) { return z(d.id); });

      legend.append("text")
        .text(function(d) { return d.id; })
        .attr("x", function(d, i) {return i*150 + margin.left + 60; })
        .attr("y", height + margin.top + margin.bottom - 5);
        
      var mouseG = g.append("g")
        .attr("class", "mouse-over-effects");
  
      mouseG.append("path") // this is the black vertical line to follow mouse
        .attr("class", "mouse-line")
        .style("stroke", "black")
        .style("stroke-width", "1px")
        .style("opacity", "0");
        
      var lines = document.getElementsByClassName('line');
  
      var mousePerLine = mouseG.selectAll('.mouse-per-line')
        .data(datas)
        .enter()
        .append("g")
        .attr("class", "mouse-per-line");
  
      mousePerLine.append("circle")
        .attr("r", 2)
        .style("stroke", function(d) {
          return z(d.id);
        })
        .style("fill", "none")
        .style("stroke-width", "1px")
        .style("opacity", "0");
      
      var barHeight = 20;
      var bar = mousePerLine.append("g")
        .attr("transform", "translate(10,2)");
      
      bar.append("rect")
        .attr("width", 30)
        .attr("height", barHeight - 1)
        .attr('fill', function(d) {
          return z(d.id);
        })
        .style("opacity", "0");
      
      bar.append("text")
        .attr("class", "y-text")
        .attr("transform", "translate(2," + barHeight/2 + ")")
        .attr("dy", ".35em")
        .style("opacity", "0");
        
      var bottomBar = mouseG.selectAll(".bar-bottom")
        .data([1])
        .enter()
        .append("g")
        .attr("class", "bar-bottom")
        .attr("transform", "translate(10," + (height - margin.bottom - margin.top) + ")");
        
      bottomBar.append("rect")
        .attr("width", 30)
        .attr("height", barHeight - 1)
        .attr('fill', "none")
        .style("opacity", "0");
      
      bottomBar.append("text")
        .attr("class", "x-text")
        .attr("transform", "translate(2," + barHeight/2 + ")")
        .attr("dy", ".35em")
        .style("opacity", "0");
  
      mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
        .attr('width', width) // can't catch mouse events on a g element
        .attr('height', height)
        .attr('fill', 'none')
        .attr('pointer-events', 'all')
        .on('mouseout', function() { // on mouse out hide line, circles and text
          d3.select(".mouse-line")
            .style("opacity", "0");
          d3.selectAll(".mouse-per-line circle")
            .style("opacity", "0");
          d3.selectAll(".mouse-per-line text")
            .style("opacity", "0");
          d3.selectAll(".mouse-per-line rect")
            .style("opacity", "0");
          d3.selectAll(".bar-bottom rect")
            .style("opacity", "0");
          d3.selectAll(".bar-bottom text")
            .style("opacity", "0");
        })
        .on('mouseover', function() { // on mouse in show line, circles and text
          d3.select(".mouse-line")
            .style("opacity", "1");
          d3.selectAll(".mouse-per-line circle")
            .style("opacity", "1");
          d3.selectAll(".mouse-per-line text")
            .style("opacity", "1");
          d3.selectAll(".mouse-per-line rect")
            .style("opacity", "1");
          d3.selectAll(".bar-bottom rect")
            .style("opacity", "1");
          d3.selectAll(".bar-bottom text")
            .style("opacity", "1");
        })
        .on('mousemove', function() { // mouse moving over canvas
          var mouse = d3.mouse(this);
          d3.select(".mouse-line")
            .attr("d", function() {
              var d = "M" + mouse[0] + "," + height;
              d += " " + mouse[0] + "," + 0;
              return d;
            });
  
          d3.selectAll(".mouse-per-line")
            .attr("transform", function(d, i) {
              var xDate = x.invert(mouse[0]),
                  bisect = d3.bisector(function(d) { return d.date; }).right,
                  idx = bisect(d.values, xDate);
              
              var beginning = 0,
                  end = lines[i].getTotalLength(),
                  target = null;
  
              while (true){
                var target = Math.floor((beginning + end) / 2);
                var pos = lines[i].getPointAtLength(target);
                if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                    break;
                }
                if (pos.x > mouse[0])      end = target;
                else if (pos.x < mouse[0]) beginning = target;
                else break; //position found
              }
              
              d3.select(this).select('.y-text')
                .text(y.invert(pos.y).toFixed(2));
              
              var format = d3.time.format("%Y-%m-%d");
              
              d3.select(".bar-bottom").select('.x-text')
                .text(format(x.invert(pos.x)));
                
              d3.select(".bar-bottom")
                .attr("transform", "translate(" + mouse[0] + "," + (height - margin.top) + ")");
                
              return "translate(" + mouse[0] + "," + pos.y +")";
            });
        })
        .on("click", function(e){
          var mouse = d3.mouse(this);
          
          var beginning = 0,
              end = lines[0].getTotalLength(),
              target = null;

          while (true){
            var target = Math.floor((beginning + end) / 2);
            var pos = lines[0].getPointAtLength(target);
            if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                break;
            }
            if (pos.x > mouse[0])      end = target;
            else if (pos.x < mouse[0]) beginning = target;
            else break; //position found
          }
          
          console.log(x.invert(pos.x));
        });
    });
    
    function type(d, a, columns) {
      d.date = parseTime(d.date);
      for (var i = 1, n = d.length, c; i < n; ++i) d[c = d[i]] = +d[c];
      return d;
    }
  }
});