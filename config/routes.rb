Rails.application.routes.draw do
  
  get 'chart', to: 'home#chart'
  get 'linechart', to: 'home#linechart'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
